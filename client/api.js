const BASE_URL = "http://localhost:3000";

function extractJsonFromBody(response) {
  return response.json();
}

export function fetchLaptops() {
  return fetch(`${BASE_URL}/laptops`).then(extractJsonFromBody);
}

export function fetchLaptopById(id) {
  return fetch(`${BASE_URL}/laptops/${id}`).then(extractJsonFromBody);
}
