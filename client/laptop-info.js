export class AppLaptopInformation {
  elLaptopPrice = document.getElementById("laptop-price");
  elImgContainer = document.getElementById("info-img-container");
  elLaptopDescriptionTxt = document.getElementById("laptop-description-txt");
  elLaptopNameTxt = document.getElementById("laptop-name-txt");
  elFeaturesList = document.getElementById("features-list");

  setLaptop(laptop) {
    this.laptop = laptop;
    this.render();
  }

  render() {
    if (!this.laptop) {
      return;
    }

    // Clear the current laptop
    //this.elLaptopInformation.innerHTML = "";
    this.elImgContainer.innerHTML = "";

    // // Create a new container for the laptop to be rendered in.
    // const elLaptop = document.createElement("div");

    const image = new Image();
    image.width = 250;
    image.height = 250;
    image.src = this.laptop.image;
    image.onload = () => {
      this.elImgContainer.appendChild(image);
    };

    this.elLaptopPrice.innerText = `NOK ${this.laptop.price}`;
    this.elLaptopNameTxt.innerText = this.laptop.name;
    this.elLaptopDescriptionTxt.innerText = this.laptop.description;

    this.elFeaturesList.innerHTML = `<ul>${this.laptop.features
      .map(function (feature) {
        return `<li>${feature}</li>`;
      })
      .join("")}</ul>`;
  }
}
