import { AppLaptop } from "./laptop.js";
import { Laptop } from "./laptop.js";

// let bankBalance = 0;
// let payBalance = 0;
// let loanBalance = 0;
// let canLoan = true;
// let hasLoan = false;

// const elLoanBtn = document.getElementById("loan-btn");
// const elBankBtn = document.getElementById("bank-btn");
// const elWorkBtn = document.getElementById("work-btn");
// const elPayBackBtn = document.getElementById("pay-back-btn");
// const elPayBalanceTxt = document.getElementById("pay-balance-txt");
// const elBankBalanceTxt = document.getElementById("bank-balance-txt");
// const elLoanBalanceTxt = document.getElementById("loan-balance-txt");

// elWorkBtn.onclick = () => work();
// elBankBtn.onclick = () => bank();
// elLoanBtn.onclick = () => loan();
// elPayBackBtn.onclick = () => payBack();

// function work() {
//   payBalance += 100;
//   //elPayBalanceTxt.innerHTML = payBalance.toString();
//   render();
// }

// function bank() {
//   if (payBalance === 0) {
//     alert("You have no money to deposit. Try working a bit");
//   } else {
//     bankBalance += payBalance;
//     payBalance = 0;
//     render();
//   }
// }

// function loan() {
//   if (bankBalance === 0) {
//     alert("You need money in your account to qualify for a loan!");
//   } else if (canLoan === false) {
//     alert(
//       "You have already loaned money. You have to buy something before you can get a new one"
//     );
//   } else if (hasLoan === true) {
//     alert(
//       "You have already loaned money. Pay back before you can get a new one"
//     );
//   } else {
//     let loanAmount = prompt(
//       "How much do you want to loan today?\nMaximum loan amount is twice your current balance",
//       "0"
//     );
//     if (loanAmount > 2 * bankBalance) {
//       alert(`You are not allowed to loan more than twice your bank balance`);
//     } else {
//       bankBalance += Number(loanAmount);
//       loanBalance += Number(loanAmount);
//       hasLoan = true;
//       canLoan = false;
//       render();
//       alert(`You got a loan of ${loanAmount}!
//       You now have a total of ${bankBalance} in your account`);
//     }
//   }
// }

// function payBack() {
//   if (payBalance > 0 && loanBalance > 0) {
//     const payBackAmount = loanBalance;
//     if (loanBalance - payBalance <= 0) {
//       loanBalance = 0;
//       hasLoan = false;
//     } else {
//       loanBalance -= payBalance;
//     }
//     payBalance -= payBackAmount;
//   }
//   console.log(loanBalance);
//   render();
// }

// function render() {
//   if (loanBalance > 0) {
//     elLoanBalanceTxt.innerText = `Loan Balance: ${loanBalance.toString()}`;
//     elPayBackBtn.style.display = "inline-block";
//   } else {
//     elLoanBalanceTxt.innerText = "";
//     elPayBackBtn.style.display = "none";
//   }

//   elBankBalanceTxt.innerText = `Account Balance: ${bankBalance.toString()}`;
//   elPayBalanceTxt.innerText = `Pay Balance: ${payBalance.toString()}`;
// }

class App {
  bankBalance = 0;
  payBalance = 0;
  loanBalance = 0;
  canLoan = true;
  hasLoan = false;

  elLoanBtn = document.getElementById("loan-btn");
  elBuyBtn = document.getElementById("buy-btn");
  elBankBtn = document.getElementById("bank-btn");
  elWorkBtn = document.getElementById("work-btn");
  elPayBackBtn = document.getElementById("pay-back-btn");
  elPayBalanceTxt = document.getElementById("pay-balance-txt");
  elBankBalanceTxt = document.getElementById("bank-balance-txt");
  elLoanBalanceTxt = document.getElementById("loan-balance-txt");
  constructor() {
    this.laptop = new AppLaptop();
    this.elBuyBtn.onclick = () => this.buyLaptop();
    this.elWorkBtn.onclick = () => this.work();
    this.elBankBtn.onclick = () => this.bank();
    this.elLoanBtn.onclick = () => this.loan();
    this.elPayBackBtn.onclick = () => this.payBack();
  }

  async init() {
    await this.laptop.init();
    this.render();
  }

  //HERE START OF PASTE

  work() {
    this.payBalance += 100;
    //elPayBalanceTxt.innerHTML = payBalance.toString();
    this.render();
  }

  bank() {
    if (this.payBalance === 0) {
      alert("You have no money to deposit. Try working a bit");
    } else {
      if (this.loanBalance > 0) {
        const bankFee = this.payBalance / 10;
        this.payBalance -= bankFee;
        this.loanBalance - bankFee <= 0
          ? (this.loanBalance = 0)
          : (this.loanBalance -= bankFee);
      }
      this.bankBalance += this.payBalance;
      this.payBalance = 0;
      this.render();
    }
  }

  loan() {
    if (this.bankBalance === 0) {
      alert("You need money in your account to qualify for a loan!");
    } else if (this.canLoan === false) {
      alert(
        "You have already loaned money. You have to buy something before you can get a new one"
      );
    } else if (this.hasLoan === true) {
      alert(
        "You have already loaned money. Pay back before you can get a new one"
      );
    } else {
      let loanAmount = prompt(
        "How much do you want to loan today?\nMaximum loan amount is twice your current balance",
        "0"
      );
      if (loanAmount === null) {
        return;
      } else if (loanAmount > 2 * this.bankBalance) {
        alert(`You are not allowed to loan more than twice your bank balance`);
      } else {
        this.bankBalance += Number(loanAmount);
        this.loanBalance += Number(loanAmount);
        this.hasLoan = true;
        this.canLoan = false;
        this.render();
        alert(`You got a loan of ${loanAmount}!
      You now have a total of ${this.bankBalance} in your account`);
      }
    }
  }

  payBack() {
    if (this.payBalance > 0 && this.loanBalance > 0) {
      const payBackAmount = this.loanBalance;
      if (this.loanBalance - this.payBalance <= 0) {
        this.loanBalance = 0;
        this.hasLoan = false;
      } else {
        this.oanBalance -= this.payBalance;
      }
      this.payBalance -= payBackAmount;
    }
    console.log(this.loanBalance);
    this.render();
  }

  buyLaptop() {
    let currentLaptop = this.laptop.selectedLaptop;

    if (this.bankBalance < currentLaptop.price) {
      alert("You don't have enough money to buy this laptop");
    } else {
      alert(`Congratulations. You now own a ${currentLaptop.name}`);
      this.bankBalance -= currentLaptop.price;
      this.canLoan = true;
      this.render();
    }
  }

  render() {
    if (this.loanBalance > 0) {
      this.elLoanBalanceTxt.innerText = `Loan Balance: ${this.loanBalance.toString()}`;
      this.elPayBackBtn.style.display = "inline-block";
    } else {
      this.elLoanBalanceTxt.innerText = "";
      this.elPayBackBtn.style.display = "none";
    }

    this.elBankBalanceTxt.innerText = `Account Balance: ${this.bankBalance.toString()}`;
    this.elPayBalanceTxt.innerText = `Pay Balance: ${this.payBalance.toString()}`;
  }
}

new App().init();
