import { fetchLaptops } from "./api.js";
import { AppLaptopInformation } from "./laptop-info.js";

export class AppLaptop {
  // HTML Elements
  elLaptopSelect = document.getElementById("laptops");

  // Data properties
  laptops = [];
  selectedLaptop = null;

  // Laptop components
  selectedLaptopInformation = new AppLaptopInformation();

  // Status properties
  error = "";

  async init() {
    this.createEventListeners();
    await this.addLaptopsToSelect();
    this.render();
  }

  async addLaptopsToSelect() {
    try {
      this.elLaptopSelect.disabled = true;
      this.laptops = await fetchLaptops().then((laptops) =>
        laptops.map((laptop) => new Laptop(laptop))
      );
    } catch (e) {
      this.error = e.message;
      console.log(e);
    }
  }

  getLaptop() {
    return this.selectedLaptop;
  }

  createEventListeners() {
    // Event Listeners
    this.elLaptopSelect.addEventListener(
      "change",
      this.onLaptopChange.bind(this)
    );
  }

  onLaptopChange() {
    if (parseInt(this.elLaptopSelect.value) === -1) {
      // Reset the current laptop.
      return;
    }

    const selectedLaptop = this.laptops.find((laptop) => {
      return laptop.id == this.elLaptopSelect.value;
    });

    this.selectedLaptop = selectedLaptop;
    this.selectedLaptopInformation.setLaptop(selectedLaptop);
  }

  createDefaultOptionForSelect() {
    const elLaptop = document.createElement("option");
    elLaptop.innerText = "-- Select a laptop --";
    elLaptop.value = -1;
    this.elLaptopSelect.appendChild(elLaptop);
  }

  render() {
    //this.elLaptopSelect.innerHTML = "";
    this.createDefaultOptionForSelect();

    this.laptops.forEach((laptop) => {
      this.elLaptopSelect.appendChild(laptop.createLaptopSelectOption());
    });

    this.elLaptopSelect.disabled = false;

    this.selectedLaptopInformation.render();
  }
}

export class Laptop {
  constructor(laptop) {
    this.id = laptop.id;
    this.name = laptop.name;
    this.price = laptop.price;
    this.description = laptop.description;
    this.image = laptop.image;
    this.features = laptop.features;
  }

  createLaptopSelectOption() {
    const elLaptopOption = document.createElement("option");
    elLaptopOption.value = this.id;
    elLaptopOption.innerText = this.name;
    return elLaptopOption;
  }
}
